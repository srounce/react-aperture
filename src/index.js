import React, { Component } from 'react'
import { createPortal, findDOMNode } from 'react-dom'
import getDisplayName from 'react-display-name'
import fastdom from 'fastdom'
import isEqual from 'lodash.isequal'
import defaultsDeep from 'lodash.defaultsdeep'
import BaseStyles from './index.css'

const calculatePositionStyle = parentDOMElement => {
  const parentBoundsRect = parentDOMElement.getBoundingClientRect()

  return {
    left: `${parentBoundsRect.left}px`,
    top: `${parentBoundsRect.top}px`,
    width: `${parentBoundsRect.width}px`,
    height: `${parentBoundsRect.height}px`
  }
}

const debounceAnimationFrame = fn => (...fnArgs) => {
  let timerId = null

  if (timerId) {
    cancelAnimationFrame(timerId)
    timerId = null
  }

  timerId = requestAnimationFrame(() => fn(...fnArgs))
}

const startAnimLoop = (fn, stopFn) => {
  requestAnimationFrame(() => {
    if (stopFn && !stopFn()) {
      return
    }
    fn()
    startAnimLoop(fn)
  })
}

export const DefaultOptions = {
  styles: {
    portal: {},
    portalPosition: {}
  },
  classNames: BaseStyles
}

export const Aperture = (parent = document.body, userOptions = {}) => {
  const config = defaultsDeep({}, userOptions, DefaultOptions)

  const portalRoot = document.createElement('div')
  portalRoot.setAttribute('class', config.classNames.portal)
  portalRoot.setAttribute('style', config.styles.portal)
  portalRoot.setAttribute('data-portal', true)
  let portalRootReady = true

  const portalClickListeners = new Set()
  const onClickPortalRoot = ev => {
    const isRootOrChildren =
      ev.target === portalRoot ||
      Array.from(portalRoot.children).includes(ev.target)

    if (isRootOrChildren) {
      for (let listener of portalClickListeners) {
        listener(ev)
      }
    }
  }
  portalRoot.addEventListener('click', onClickPortalRoot)
  parent.appendChild(portalRoot)

  const elementListeners = new Set()
  const onResize = debounceAnimationFrame(() => {
    for (let listener of elementListeners) {
      listener()
    }
  })
  startAnimLoop(onResize, () => portalRootReady)

  const setPointerEvents = () => {
    if (portalRoot.children.length) {
      portalRoot.style.pointerEvents = 'auto'
    } else {
      portalRoot.style.pointerEvents = 'none'
    }
  }

  const parentMutationObserver = new MutationObserver(() => {
    if (!parent.hasChildNodes(portalRoot)) {
      portalRootReady = false
      portalRoot.removeEventListener(onClickPortalRoot)
    }
  })
  parentMutationObserver.observe(parent, { childList: true })

  const rootMutationObserver = new MutationObserver(() => {
    setPointerEvents()
  })
  rootMutationObserver.observe(portalRoot, { childList: true })
  setPointerEvents()

  const wrap = InnerComponent => {
    class PortalComponent extends Component {
      ready = false
      state = {
        parentDOMElement: null
      }

      static getDerivedStateFromProps (newProps, oldState) {
        const { 'portal-parent': portalParent } = newProps

        if (typeof portalParent === 'undefined') {
          throw new Error('Error: `portal-parent` property not set')
        }

        if (!portalParent.current) {
          return null
        }

        if (portalParent.current === oldState.parentDOMElement) {
          return null
        }

        const parentDOMElement =
          portalParent.current instanceof HTMLElement
            ? portalParent.current
            : findDOMNode(portalParent.current)

        return {
          parentDOMElement,
          positionStyle: calculatePositionStyle(parentDOMElement)
        }
      }

      componentDidMount () {
        elementListeners.add(this.onPortalElementUpdated)
        portalClickListeners.add(this.onPortalRootClicked)

        this.ready = true
        this.forceUpdate()
      }

      componentWillUnmount () {
        this.ready = false

        elementListeners.delete(this.onPortalElementUpdated)
        portalClickListeners.delete(this.onPortalRootClicked)
      }

      onPortalElementUpdated = newStyle => {
        const { parentDOMElement, positionStyle: oldPositionStyle } = this.state
        let positionStyle = oldPositionStyle

        fastdom.measure(() => {
          if (this.ready && parentDOMElement) {
            positionStyle = calculatePositionStyle(parentDOMElement)
          }
        })

        fastdom.mutate(() => {
          if (this.ready && !isEqual(oldPositionStyle, positionStyle)) {
            this.setState({ positionStyle })
          }
        })
      }

      onPortalRootClicked = ev => {
        const { onClickOutside } = this.props

        if (typeof onClickOutside === 'function') {
          onClickOutside(ev)
        }
      }

      render () {
        const {
          children,
          onClickOutside: __,
          'portal-parent': _,
          ...childProps
        } = this.props
        const { parentDOMElement, positionStyle } = this.state

        if (!this.ready || !parentDOMElement) {
          return null
        }

        const portalPositionStyle = {
          ...config.styles.portalPosition,
          ...positionStyle
        }

        const portalContent = (
          <div
            className={config.classNames.portalPosition}
            style={portalPositionStyle}
          >
            <InnerComponent {...childProps}>{children}</InnerComponent>
          </div>
        )

        return createPortal(portalContent, portalRoot)
      }
    }
    PortalComponent.displayName = `Aperture(${getDisplayName(InnerComponent)})`

    return PortalComponent
  }

  return wrap
}

export default Aperture

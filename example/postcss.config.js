module.exports = {
  modules: true,
  plugins: {
    'postcss-import': {},
    'postcss-nested-import': {},
    'postcss-nested': {},
    /* 'postcss-modules': {
         generateScopedName: (name, filepath, css) => {
           const i = css.indexOf(`${name}`);
           const splitCSS = css.split(/[\r\n]/)
           const lineNumber = css.substr(0, i).split(/[\r\n]/).length
           let path = filepath
             .replace(__dirname, '')
             .replace(/\\/g, '/')
             .replace(/([.:])/g, '\\$1')
             .split('/')
             .slice(1)
             .join('/')

           const cssLine = splitCSS[lineNumber - 1]

           if (cssLine.match(/(@keyframes|@media)/)) {
             return name
           }

           let output = `${name}__${path}\\:${lineNumber}`

           return output
         }
    },*/ 
    autoprefixer: {
      browsers: ['>5%']
    }
  }
}


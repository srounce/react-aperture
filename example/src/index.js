import React, { Component } from "react";
import { render } from "react-dom";
import Color from "color";
import classnames from "classnames";

import Aperture from "react-aperture";
import styles from "./index.css";

const Overlay = ({ color: colorProp, children, ...otherProps }) => {
  let color = colorProp;
  if (!color || !color.length) {
    color = "grey";
  }
  const backgroundColor = Color(color)
    .rotate(180)
    .darken(0.25)
    .desaturate(0.25)
    .string();

  return (
    <div className={styles.overlay} style={{ backgroundColor }} {...otherProps}>
      {children}
      <div className={styles.secretText}>THIS IS SECRET "{children}"</div>
    </div>
  );
};

const portal = Aperture();
const OverlayPortal = portal(Overlay);

class Hello extends Component {
  constructor(props) {
    super(props);

    this.containerRef = React.createRef();
    this.state = {
      overlayVisible: false
    };
  }

  setOverlayVisible(newState) {
    this.setState({ overlayVisible: newState });
  }

  render() {
    const { name, color: colorProp } = this.props;
    const { overlayVisible } = this.state;
    let color = colorProp;
    if (!color || !color.length) {
      color = "grey";
    }
    color = Color(color)
      .darken(0.25)
      .saturate(0.25)
      .string();

    const background = Color(colorProp)
      .lighten(0.25)
      .desaturate(0.25)
      .string();

    return (
      <div
        className={styles.helloItem}
        style={{ background, color }}
        ref={this.containerRef}
        onClick={() => this.setOverlayVisible(true)}
      >
        Hello {name}
        {overlayVisible && (
          <OverlayPortal
            portal-parent={this.containerRef}
            color={colorProp}
            onClickOutside={ev => {
              ev.preventDefault();
              this.setOverlayVisible(false);
            }}
          >
            {`Overlay content for ${name}`}
          </OverlayPortal>
        )}
      </div>
    );
  }
}

class App extends Component {
  state = { animated: false };

  render() {
    const { animated } = this.state;

    return (
      <div className={classnames(styles.app, { [styles.animated]: animated })}>
        <ul className={styles.controls}>
          <li>
            <label>
              Animated:
              <input
                type="checkbox"
                checked={animated}
                onChange={() => {
                  this.setState({ animated: !animated });
                }}
              />
            </label>
          </li>
        </ul>
        <div className={styles.sandbox}>
          <Hello name="Dolly" color="red" />
          <Hello name="World" color="green" />
          <Hello name="Universe" color="blue" />
        </div>
      </div>
    );
  }
}

render(<App />, document.getElementById("app"));

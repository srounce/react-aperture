module.exports = {
  parser: 'babel-eslint',
  extends: ['standard', 'plugin:react/recommended', 'plugin:jest/recommended'],
  plugins: ['jest'],
  env: {
    browser: true
  },
  rules: {
    'react/prop-types': [0],
    'multiline-ternary': 0,
    'react/no-find-dom-node': 0,
    'max-len': ['error', { code: 80 }]
  }
}
